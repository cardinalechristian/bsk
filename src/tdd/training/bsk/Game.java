package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private ArrayList<Frame> framesList;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		framesList = new ArrayList<Frame>(10);
	}
	
	
	/**
	 * It return the conteiner of frames
	 */
	public ArrayList<Frame> getFramesList() {
		return framesList;
	}


	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		framesList.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return framesList.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int score = 0;
		int index = 0;
		
		for(Frame frame : framesList) {
		
			
			if((frame.isStrike() && index == 8) && framesList.get(index + 1).isStrike()) {
				frame.setBonus(framesList.get(index + 1).getFirstThrow() + firstBonusThrow);
				score += frame.getScore();
			}else if(frame.isSpare() && index == 9) {
				frame.setBonus(firstBonusThrow);
				score += frame.getScore();
			}else if(frame.isStrike() && index == 9) {
				frame.setBonus(firstBonusThrow + secondBonusThrow);
				score += frame.getScore();
			}else if(frame.isSpare()) {
				frame.setBonus(framesList.get(index + 1).getFirstThrow());
				score += frame.getScore();
			}else if(frame.isStrike() && framesList.get(index + 1).isStrike()) {
				frame.setBonus(framesList.get(index + 1).getFirstThrow() + framesList.get(index + 2).getFirstThrow());
				score += frame.getScore();
			}else if(frame.isStrike()) {
				frame.setBonus(framesList.get(index + 1).getFirstThrow() + framesList.get(index + 1).getSecondThrow());
				score += frame.getScore();
			}else {
				score += frame.getScore();
			}
			
			index++;
		}
		
		return score;	
	}


}
