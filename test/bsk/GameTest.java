package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void addFrameToGameTest() throws Exception {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		Frame frame0 = new Frame(4,3);
		frames.add(frame0);
		game.addFrame(frame0);
		assertEquals(frames,game.getFramesList());
	}
	
	
	@Test
	public void getSpecificFrameTest() throws Exception {
		Game game = new Game();
		Frame frame0 = new Frame(4,3);
		game.addFrame(frame0);
		assertEquals(frame0,game.getFrameAt(0));
	}
	
	
	@Test
	public void getSpecificFrameTest2() throws Exception {
		Game game = new Game();
		Frame frame0 = new Frame(4,3);
		Frame frame1 = new Frame(1,7);
		game.addFrame(frame0);
		game.addFrame(frame1);
		assertEquals(frame1,game.getFrameAt(1));
	}
	
	
	@Test
	public void calculateGameScoreTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(81,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithSpareTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(88,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithStrikeTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(94,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithStrikeFollowedBySpareTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(103,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithStrikeFollowedByStrikeTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(112,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithSpareFollowedBySpareTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(98,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithSpareAsTheLastFrameTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
		assertEquals(90,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithStrikeAsTheLastFrameTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92,game.calculateScore());
	}
	
	
	@Test
	public void calculateGameScoreWithBestScoreTest() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300,game.calculateScore());
	}

}
