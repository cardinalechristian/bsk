package bsk;

import static org.junit.Assert.*;

import tdd.training.bsk.Frame;

import org.junit.Test;


public class FrameTest {

	@Test
	public void getFirstThrowTest() throws Exception{
		Frame frame = new Frame(3,0);
		assertEquals(3,frame.getFirstThrow());
	}
	
	
	@Test
	public void getSecondThrowTest() throws Exception{
		Frame frame = new Frame(2,5);
		assertEquals(5,frame.getSecondThrow());
	}
	
	
	@Test
	public void getScoreFrameTest() throws Exception{
		Frame frame = new Frame(2,5);
		assertEquals(7,frame.getScore());
	}
	
	@Test
	public void isSpareFrameTest() throws Exception{
		Frame frame = new Frame(6,4);
		assertTrue(frame.isSpare());
	}

	
	@Test 
	public void getScoreWithBonusSpareTest() throws Exception{
		Frame frame = new Frame(6,4);
		frame.setBonus(4);
		assertEquals(14,frame.getScore());
	}
	
	
	@Test
	public void isStrikeFrameTest() throws Exception{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	
	@Test
	public void getScoreWithBonusStrikeTest() throws Exception{
		Frame frame = new Frame(10,0);
		frame.setBonus(6);
		assertEquals(16,frame.getScore());
	}
}
